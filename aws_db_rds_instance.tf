#-----------------------------------------------------------------------------------------------------
# DB subnet group
#-----------------------------------------------------------------------------------------------------

resource "aws_db_subnet_group" "rds" {
  name       = "${var.environment}-rds_db"
  subnet_ids = [var.subnet_private_id, var.subnet_private_id2]

  tags = {
    Name = "My DB subnet group"
  }
}

#-----------------------------------------------------------------------------------------------------
# RDS
#-----------------------------------------------------------------------------------------------------

resource "aws_db_instance" "default" {
  allocated_storage     = var.db_allocated_storage
  max_allocated_storage = var.db_max_allocated_storage
  storage_type          = var.db_storage_type
  instance_class        = var.db_instance_class

  engine               = var.db_engine
  parameter_group_name = var.db_parameter_group_name
  engine_version       = var.db_engine_version
  port                 = var.db_port

  identifier = var.db_identifier
  name       = var.db_name
  username   = var.db_root_username
  password   = var.db_root_password

  deletion_protection         = var.db_deletion_protection
  vpc_security_group_ids      = [var.instance_sg_id]
  backup_retention_period     = var.db_backup_retention_period
  allow_major_version_upgrade = var.db_allow_major_version_upgrade
  auto_minor_version_upgrade  = var.db_auto_minor_version_upgrade
  db_subnet_group_name        = aws_db_subnet_group.rds.id
  publicly_accessible         = var.db_publicly_accessible

  skip_final_snapshot       = var.db_skip_final_snapshot
  final_snapshot_identifier = var.db_final_snapshot_identifier

  tags = {
    Project     = var.project_name
    Environment = var.environment
  }
}
