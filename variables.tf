#-----------------------------------------------------------------------------------------------------
# GLOBAL VARIABLES
#-----------------------------------------------------------------------------------------------------

variable "project_name" {
  type = string
}

variable "environment" {
  type = string
}

variable "aws_region" {
  type = string
}

#-----------------------------------------------------------------------------------------------------
# VPC VARIABLES
#-----------------------------------------------------------------------------------------------------

variable "subnet_private_id" {
  type = string
}

variable "subnet_private_id2" {
  type = string
}
#-----------------------------------------------------------------------------------------------------
# VPC VARIABLES
#-----------------------------------------------------------------------------------------------------

variable "instance_sg_id" {
  type = string
}

variable "db_identifier" {
  type = string
}

variable "db_name" {
  type = string
}

variable "db_root_username" {
  type = string
}

variable "db_final_snapshot_identifier" {
  type = string
}


#-----------------------------------------------------------------------------------------------------
# RDS VARIABLES
#-----------------------------------------------------------------------------------------------------

variable "db_instance_class" {
  type = string
}

variable "db_storage_type" {
  type = string
}

variable "db_allocated_storage" {
  type = number
}

variable "db_max_allocated_storage" {
  type = number
}

variable "db_root_password" {
  type = string
}

variable "db_engine" {
  type = string
}

variable "db_parameter_group_name" {
  type = string
}

variable "db_engine_version" {
  type = string
}

variable "db_port" {
  type = number
}

variable "db_backup_retention_period" {
  type = number
}

variable "db_skip_final_snapshot" {
  type = bool
}

variable "db_deletion_protection" {
  type = bool
}

variable "db_allow_major_version_upgrade" {
  type = bool
}

variable "db_auto_minor_version_upgrade" {
  type = bool
}

variable "db_publicly_accessible" {
  type = bool
}
