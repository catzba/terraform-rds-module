output "db_endpoint" {
  value = aws_db_instance.default.endpoint
}

output "db_name" {
  value = aws_db_instance.default.name
}

output "db_root_username" {
  value = aws_db_instance.default.username
}

output "db_root_password" {
  value = var.db_root_password
}

output "db_identifier" {
  value = var.db_identifier
}

output "db_engine" {
  value = aws_db_instance.default.engine
}

output "db_engine_version" {
  value = aws_db_instance.default.engine_version
}

output "db_instance_class" {
  value = aws_db_instance.default.instance_class
}
